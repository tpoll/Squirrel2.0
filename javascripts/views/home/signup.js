define(['jquery',
    'underscore',
    'marionette',
    'velocity',
    'ui/notification',
    'ui/form',
    'text!static/home/signup.html'
], function($,
    _,
    Marionette,
    Velocity,
    Notif,
    UIForm,
    signupTpl
) {

    var signupView = Marionette.ItemView.extend({

        template: _.template(signupTpl),

        events: {
            "click #login-from-signup-button":"login",
            "click #signup-button":"submitSignup",
            "submit #signup-form":"submitSignup"
        },

        onRender: function() {
            var view = this;
            this.$el.css('opacity', 0);
            window.setTimeout(function() {
                view.$el.css('margin-top', (($(window).height() - $(".form-wrapper").height()) / 2));
                view.$el.velocity({
                    opacity: 1
                }, 400)
            }, 1)
            
        },

        login: function() {
            Backbone.history.navigate("login", {trigger:true})
        },

        submitSignup: function(e) {
            e.preventDefault();
            if(!UIForm.validateFields(['first-name','last-name','email','password'])) return;
            this.postSignup();
        },

        postSignup: function() {
            console.log('here')
            $.ajax({
                url: "/api/users",
                type: "POST",
                dataType: "json",
                data: {
                    email: $("#email").val(),
                    password: $("#password").val(),
                    first_name: $("#first-name").val(),
                    last_name: $("#last-name").val(),
                    id_card: $("#id-card").val()
                },
                success: function(data) {
                    Notif.botSuccess("Welcome to Squirrel", 2000);
                    localStorage.setItem("userSignedIn", true);
                    Backbone.history.navigate("dashboard", {trigger: true});
                },
                error: function(header, status, error) {
                    var code = header.status
                    if(code == 500 || code == 400) {
                        Notif.botError(header["responseText"], 4000)
                    } else {
                        Notif.botError("An error occurred", 4000)
                    }
                }
            })
        }

    })

    return signupView;

})