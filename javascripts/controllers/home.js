define(['jquery',
    'underscore',
    'marionette',
    'models/event',
    'collections/events',
    'ui/notification',
    'views/home/login',
    'views/home/signup',
    'views/dashboard/dashboard_layout',
    'views/events/events',
    'views/events/event_page'
], function($, 
    _, 
    Marionette,
    Event,
    EventsCollection,
    Notif,
    loginView,
    signupView,
    dashboardLayout,
    eventsView,
    eventPageLayout
) {


    var homeController = Marionette.Controller.extend({

        login: function() {
            if(Backbone.history.fragment != "login") {
                Backbone.history.navigate("login", {trigger: false});
            }
            if(!localStorage.getItem("userId")) {
                window.Layout.content.show(new loginView())
            } else {
                Backbone.history.navigate("dashboard", {trigger: true})
            }
        },

        signup: function() {
            if(!localStorage.getItem("userId")) {
                window.Layout.content.show(new signupView())
            } else {
                Backbone.history.navigate("dashboard", {trigger: true})
            }
        },

        logout: function() {
            $.ajax({
                url: '/api/logout',
                type: 'post',
                dataType: 'json',
                success: function(data) {
                    window.Layout.navigation.currentView.disappear();
                    localStorage.removeItem("userId");
                    Backbone.history.navigate("login", {trigger: true});
                    Notif.botSuccess("Logged Out", 1000);
                }, 
            })
            
        },

        dash: function() {
            if(!localStorage.getItem("userId")) {
                Backbone.history.navigate("login", {trigger:true})
            } else {
                if(window.Layout.navigation.currentView.$el.hasClass('disappeared')) {
                    window.Layout.navigation.currentView.appear("dashboard");
                }
                window.Layout.content.show(new dashboardLayout());
            }
            
        },

        events: function() {
            if(!localStorage.getItem("userId")) {
                Backbone.history.navigate("login", {trigger:true})
            } else {
                if(window.Layout.navigation.currentView.$el.hasClass('disappeared')) {
                    window.Layout.navigation.currentView.appear("events");
                }
                var events = new EventsCollection();
                events.fetch({
                    success: function() {
                        console.log(events)
                        window.Layout.content.show(new eventsView({collection: events, title: "All Events"}));        
                    }
                })
                
            }
        },

        event: function(id) {
            if(!localStorage.getItem("userId")) {
                Backbone.history.navigate("login", {trigger:true})
            } else {
                if(window.Layout.navigation.currentView.$el.hasClass('disappeared')) {
                    window.Layout.navigation.currentView.appear();
                }
                var eventModel = new Event({id: id})
                window.Layout.content.show(new eventPageLayout({model: eventModel}))
            }
        }

    })

    return homeController

})