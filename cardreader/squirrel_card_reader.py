import sys, urllib, urllib2, pyaudio, wave
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.util import toHexString

# code to send to the card reader
APDU = [0xFF, 0xCA, 0x00, 0x00, 0x00]
# Event ID parameter to post to 
EVENT_ID = input("Event ID: ")
# URL to post to -- take EVENT ID as parameter
URL = 'http://104.131.126.99/api/events/'+str(EVENT_ID)+'/checkin'
# Identifier that ensures that only Tufts cards are being read.
ATR = [59, 143, 128, 1, 128, 79, 12, 160, 0, \
        0, 3, 6, 10, 0, 31, 0, 0, 0, 0, 125]

def play_sound():
        chunk = 1024
        wf = wave.open('squirrel_squeak.wav', 'rb')
        p = pyaudio.PyAudio()
        stream = p.open(format =
                        p.get_format_from_width(wf.getsampwidth()),
                        channels = wf.getnchannels(),
                        rate = wf.getframerate(),
                        output = True)
        data = wf.readframes(chunk)
        while data != '':
            stream.write(data)
            data = wf.readframes(chunk)
        stream.close()
        p.terminate()

def post_to_server(tagid):
        params = urllib.urlencode({'id_card':tagid})
        response = urllib2.urlopen(URL, params).read()

def main():
        known_ids = {}
        cardtype = AnyCardType()
        #times out every 0.5 seconds
        cardrequest = CardRequest( timeout=0.5, cardType=cardtype )
        while True:
                try: 
                        # try to connect to a card, if the process 
                        # times out, continue
                        cardservice = cardrequest.waitforcard()
                        cardservice.connection.connect()
                        # compare ATRs
                        if ATR == cardservice.connection.getATR():
                                response, sw1, sw2 = \
                                    cardservice.connection.transmit( APDU )
                                tagid = toHexString(response).replace(' ','')
                                if tagid in known_ids:
                                        print "found uid"
                                        print tagid
                                else:
                                        known_ids[tagid] = True
                                        print "new uid"
                                        print tagid
                                # do appropriate things
                                post_to_server(tagid)
                                play_sound()
                except:
                        continue

main()

