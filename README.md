for local (need to change port to):

Startup server by running ./startup or 
go run src/squirrels/router/router.go from the root of the squirrel2 directory

Go to http://104.131.126.99/ for a live version of the website

Our code is broken up into 6 root folders. bin, pkg, and src are the typical
folders a go project will have. bin would have compiled source. pkg, contains
whatever packages are used, and src contains the actual source code
for the entire project. Cardreader contains the logic for the separate card 
reading source. hardware is required.

The file “squirrel_card_reader.py” is the reading and REST-ful posting 
program that checks in attendees to events. Run it by installing 
the PyAudio and SmartCard library (http://pyscard.sourceforge.net/) 
and running it via Python 2.6. If you would like a hardware demonstration, 
feel free to contact Nikhil at nikhilshinday@gmail.com to meet!

Javascripts and sass are the folders where almost all of the code for the front
end is stored. 

Within source there are 4 directories which contain the source code for all
of the packages we used. The folder if interested is squirrels which contains
the source for all of the packages we wrote.
