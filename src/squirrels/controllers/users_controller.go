// Package the handles creation, and getting a users info
package controllers

import (
	"database/sql"
	"net/http"
	"squirrels/db"
	"squirrels/models"
	"squirrels/record"
	"squirrels/sessions"
	"squirrels/validations"
	"unicode"
)

func UserCreate(w http.ResponseWriter, r *http.Request, dbLink *sql.DB) {
	email := r.FormValue("email")
	pass := r.FormValue("password")
	first := r.FormValue("first_name")
	last := r.FormValue("last_name")
	id_card := r.FormValue("id_card")

	var u record.User
	if !validations.UserExistsWithEmail(&u, email, dbLink) {
		u = record.User{
			Email:    email,
			First:    capitalize(first),
			Last:     capitalize(last),
			PassHash: models.UserCreatePassHash(pass),
			IdCard:   id_card,
		}
		if db.CreateUser(&u, dbLink) {
			sessions.Login(w, r, dbLink)
		} else {
			http.Error(w, "Could not create user", 500)
		}
	} else {
		http.Error(w, "Email already used", 400)
	}
}

func UserGet(w http.ResponseWriter, r *http.Request, dbLink *sql.DB, id int) {
	if sessions.AuthenticateBasic(r, dbLink) {
		var u record.User
		if db.ReadUser(&u, dbLink, id) {
			w.Write(models.UserToJSON(u))
		}
	} else {
		http.Error(w, "No user logged in", 403)
	}
}

func capitalize(s string) string {
	a := []rune(s)
	a[0] = unicode.ToUpper(a[0])
	return string(a)
}
