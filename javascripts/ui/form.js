define(['jquery',
    'velocity',
    'ui/notification'
], function($,
    Velocity,
    Notif
) {
        
    var form = {
        fieldAnimate: function(fields) {
            for (field in fields) {
                $(".form-control[name="+fields[field]+"]")
                .blur()
                .addClass("error")
                .on('focus', function() {
                    $(this).removeClass("error");
                })
            }
        },

        validateFields: function(fields) {
            for (field in fields) {
                if($(".form-control[name="+fields[field]+"]").val() == "") {
                    Notif.botError(fields[field].split('-').join(' ') + " cannot be blank", 1000);
                    form.fieldAnimate([fields[field]])
                    return false;
                }
            }
            return true;
        }
    }

    return form;

});