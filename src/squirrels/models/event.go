// Part of the models package converting the event struct to and from json
package models

import (
	"encoding/json"
	"log"
	"squirrels/errors"
	"squirrels/record"
)

func EventFromJSON(jsonString []byte, e *record.Event) {
	err := json.Unmarshal(jsonString, e)
	if errors.IsErr(err) {
		log.Fatalln("Could not Unmarshal Event JSON")
	}
}

func EventToJSON(e record.Event) []byte {
	json, err := json.Marshal(e)
	if errors.IsErr(err) {
		log.Fatalln("Could not Marshal Event Object")
	}
	return json
}

func EventsToJson(events []record.Event) []byte {
	json, err := json.Marshal(events)
	if errors.IsErr(err) {
		log.Fatalln("could not Marshal Event Objects")
	}
	return json
}
