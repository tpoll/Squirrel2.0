<div class="event-expanded-info">
    <div class="top-info">
        <div class="title"> <h3><%= title %> <span>hosted by</span></h3><h3 class="host"></h3></div>
        <div class="description"> <p><%= description %></p> </div>
    </div>
    <div class="bot-info">
        
        <div class="info location"> <p><%= location %></p> </div>
        <div class="info time"> <p><%= time.split('T')[0] %><span> at </span><%= time.split('T')[1] %>.</span></p></div>
        <div class="info capacity"> <p><%=cur_tickets%>/<%= max_tickets %> tickets taken</p> </div>
    </div>
    <div class="button" id="join"><p>Join Event</p></div>
</div>
