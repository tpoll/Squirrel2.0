// Part of the models package converting the attendee struct to and from json
package models

import (
	"encoding/json"
	"log"
	"squirrels/errors"
	"squirrels/record"
)

func AttendeeToJSON(attendee record.Attendee) []byte {
	json, err := json.Marshal(attendee)
	if errors.IsErr(err) {
		log.Fatalln("could not Marshal Attendee Object")
	}
	return json
}

func AttendeesToJSON(attendees []record.Attendee) []byte {
	json, err := json.Marshal(attendees)
	if errors.IsErr(err) {
		log.Fatalln("could not Marshal Attendee Objects")
	}
	return json
}
