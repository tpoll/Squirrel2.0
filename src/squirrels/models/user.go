// Part of the models package converting the use struct to and from json
// and hasing and comparing the user's hash to what we have
package models

import (
	"code.google.com/p/go.crypto/bcrypt"
	"encoding/json"
	"log"
	"squirrels/errors"
	"squirrels/record"
)

func UserFromJSON(jsonString string, u *record.User) {
	err := json.Unmarshal([]byte(jsonString), u)
	if errors.IsErr(err) {
		log.Fatalln("Could not Unmarshal User JSON")
	}
}

func UserToJSON(u record.User) []byte {
	json, err := json.Marshal(u)
	if errors.IsErr(err) {
		log.Fatalln("Could not Marshal User Object")
	}
	return json
}

func UserCreatePassHash(pass string) string {
	byteArray, _ := bcrypt.GenerateFromPassword([]byte(pass), 10)
	return string(byteArray[:])
}

func UserComparePassHash(pass string, hash string) bool {
	if bcrypt.CompareHashAndPassword([]byte(hash), []byte(pass)) == nil {
		return true
	} else {
		return false
	}
}
