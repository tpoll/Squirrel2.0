// Main package that implements http handler and listens and serves repsonses.
package main

import (
	"database/sql"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	_ "github.com/lib/pq"
	"net/http"
	"squirrels/controllers"
	"squirrels/maps"
	"squirrels/record"
	"squirrels/sessions"
	"strconv"
	"sync"
)

func displayIndex(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "index.html")
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

var connections = make(map[int]*websocket.Conn)

func main() {

	dbLink, _ := sql.Open("postgres", "postgres://xuihgygrphqoas:iSfrBpnf3ybhnmT3aZl-EVDzem@ec2-107-20-159-155.compute-1.amazonaws.com:5432/d9vbco1jv14e38")
	defer dbLink.Close()

	m := &maps.ThMap{make(map[int]maps.Semaphore), &sync.RWMutex{}}
	maps.InitMaps(m, dbLink)

	r := mux.NewRouter()

	// request handlers
	r.HandleFunc("/api/login", func(w http.ResponseWriter, r *http.Request) {
		sessions.Login(w, r, dbLink)
	}).Methods("post")

	r.HandleFunc("/api/logout", func(w http.ResponseWriter, r *http.Request) {
		sessions.Logout(w)
	}).Methods("post")

	/* User routes */
	r.HandleFunc("/api/users", func(w http.ResponseWriter, r *http.Request) {
		controllers.UserCreate(w, r, dbLink)
	}).Methods("post")

	r.HandleFunc("/api/users/{id}", func(w http.ResponseWriter, r *http.Request) {
		id, _ := strconv.Atoi(mux.Vars(r)["id"])
		controllers.UserGet(w, r, dbLink, id)
	}).Methods("get")

	/* Event routes */
	r.HandleFunc("/api/events", func(w http.ResponseWriter, r *http.Request) {
		controllers.EventCreate(w, r, dbLink, m)
	}).Methods("post")

	r.HandleFunc("/api/events", func(w http.ResponseWriter, r *http.Request) {
		controllers.EventsGet(w, r, dbLink)
	}).Methods("get")

	r.HandleFunc("/api/events/{id}", func(w http.ResponseWriter, r *http.Request) {
		id, _ := strconv.Atoi(mux.Vars(r)["id"])
		controllers.EventGet(w, r, dbLink, id)
	}).Methods("get")

	r.HandleFunc("/api/events/{id}/attendees", func(w http.ResponseWriter, r *http.Request) {
		id, _ := strconv.Atoi(mux.Vars(r)["id"])
		controllers.EventGetAttendees(w, r, dbLink, id)
	}).Methods("get")

	r.HandleFunc("/api/events/{id}", func(w http.ResponseWriter, r *http.Request) {
		id, _ := strconv.Atoi(mux.Vars(r)["id"])
		controllers.EventDestroy(w, r, dbLink, id)
	}).Methods("delete")

	r.HandleFunc("/api/events/{id}/ticket", func(w http.ResponseWriter, r *http.Request) {
		id, _ := strconv.Atoi(mux.Vars(r)["id"])
		obtained, a := controllers.EventGetTicket(w, r, dbLink, m, id)
		if obtained {
			for _, element := range connections {
				element.WriteJSON(a)
			}
		}
	}).Methods("post")

	r.HandleFunc("/api/events/{id}/checkin", func(w http.ResponseWriter, r *http.Request) {
		id, _ := strconv.Atoi(mux.Vars(r)["id"])
		var a record.Attendee
		var exists bool
		a.Person = record.User{}
		a.Info = record.Ticket{}
		exists, a = controllers.EventCheckIn(w, r, dbLink, a, id)
		if exists {
			for _, element := range connections {
				element.WriteJSON(a)
			}
		}
	}).Methods("post")

	/* Routes for static assets */
	r.PathPrefix("/javascripts/").Handler(http.StripPrefix("/javascripts/", http.FileServer(http.Dir("javascripts/"))))
	r.PathPrefix("/css/").Handler(http.StripPrefix("/css/", http.FileServer(http.Dir("css/"))))
	r.HandleFunc("/{path:.*}", displayIndex)

	http.Handle("/", r)

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			println(err)
			return
		}
		connections[sessions.GetUserId(r)] = conn
	})
	// http.ListenAndServe(":3000", nil)
	http.ListenAndServe(":80", nil)
}
