define(['jquery',
    'backbone'
], function($,
    backbone
) {

    var user = Backbone.Model.extend({

        urlRoot: '/api/users'

    })

    return user;

})