// Package the handles requests to create, join, checking, and delete
// an event.
package controllers

import (
	"database/sql"
	"io/ioutil"
	"net/http"
	"squirrels/db"
	"squirrels/maps"
	"squirrels/models"
	"squirrels/record"
	"squirrels/sessions"
	"squirrels/validations"
	"strconv"
)

func EventCreate(w http.ResponseWriter, r *http.Request, dbLink *sql.DB,
	m *maps.ThMap) {

	body, _ := ioutil.ReadAll(r.Body)
	var e record.Event
	models.EventFromJSON(body, &e)
	e.CurTickets = e.MaxTickets

	if db.CreateEvent(&e, dbLink) {
		w.Write(models.EventToJSON(e))
		maps.MakeEvent(m, e.Id, e.MaxTickets) //Creates event in memory
	} else {
		http.Error(w, "Could not create event", 500)
	}
}

func EventGet(w http.ResponseWriter, r *http.Request, dbLink *sql.DB, id int) {
	if sessions.AuthenticateBasic(r, dbLink) {
		var e record.Event
		if db.ReadEvent(&e, dbLink, id) {
			w.Write(models.EventToJSON(e))
		} else {
			http.Error(w, "No event with specified id", 404)
		}
	} else {
		http.Error(w, "No user logged in", 403)
	}
}

func EventGetAttendees(w http.ResponseWriter, r *http.Request, dbLink *sql.DB,
	event_id int) {
	if sessions.AuthenticateBasic(r, dbLink) {
		var attendees []record.Attendee
		attendees = db.ReadEventAttendees(dbLink, event_id)
		w.Write(models.AttendeesToJSON(attendees))
	} else {
		http.Error(w, "No user loged in", 403)
	}
}

func EventGetTicket(w http.ResponseWriter, r *http.Request, dbLink *sql.DB,
	m *maps.ThMap, event_id int) (bool, record.Attendee) {
	u_id, err := strconv.Atoi(r.FormValue("user_id"))
	var a record.Attendee
	if !validations.TicketExists(u_id, event_id, dbLink) {
		if sessions.AuthenticateStrong(r, u_id, dbLink) {
			err = maps.GetTicket(m, event_id, dbLink)
			if err == nil {
				var u record.User
				db.ReadUser(&u, dbLink, u_id)
				var t record.Ticket
				t.UserId = u_id
				t.EventId = event_id
				t.CheckedIn = false
				if db.CreateTicket(&t, dbLink) {
					a.Info = t
					a.Person = u
					a.UserId = u_id
					w.Write(models.AttendeeToJSON(a))
					return true, a
				}
			} else {
				http.Error(w, "No tickets left!", 404)
			}
		}
	} else {
		http.Error(w, "You already have a ticket!", 400)
	}
	return false, a
}

func EventCheckIn(w http.ResponseWriter, r *http.Request, dbLink *sql.DB,
	a record.Attendee, event_id int) (bool, record.Attendee) {
	u_id_str := r.FormValue("user_id")
	var u_id int
	if u_id_str == "" {
		u_id_card := r.FormValue("id_card")
		println(u_id_card)
		if db.FindUserByCardId(&a.Person, dbLink, u_id_card) {
			u_id = a.Person.Id
		} else {
			return false, a
		}
	} else {
		u_id, _ = strconv.Atoi(u_id_str)
		db.ReadUser(&a.Person, dbLink, u_id)
	}

	if db.TicketByEventAndUser(&a.Info, u_id, event_id, dbLink) {
		a.Info.CheckedIn = true
		db.UpdateTicket(&a.Info, dbLink)
		a.UserId = u_id
		return true, a
	} else {
		return false, a
	}
	return false, a
}

func EventsGet(w http.ResponseWriter, r *http.Request, dbLink *sql.DB) {

	var uidpassed bool
	uidstring := r.FormValue("user_id")
	uidpassed = uidstring != ""
	uid, _ := strconv.Atoi(uidstring)
	var events []record.Event

	if uidpassed {
		events = db.ReadEventsCreated(dbLink, uid)
	} else {
		events = db.ReadEventsAll(dbLink)
	}

	w.Write(models.EventsToJson(events))

}

func EventDestroy(w http.ResponseWriter, r *http.Request, dbLink *sql.DB,
	id int) {
	if sessions.AuthenticateBasic(r, dbLink) {
		if db.DestroyEvent(dbLink, id) {
			w.Write([]byte(`{"ok": "deleted"}`))
		} else {
			http.Error(w, "Could not delete event", 500)
		}
	} else {
		http.Error(w, "No user logged in", 403)
	}
}
