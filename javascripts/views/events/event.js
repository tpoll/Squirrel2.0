define(['jquery',
    'underscore',
    'marionette',
    'text!templates/dashboard/event.tpl'
], function($,
    _,
    Marionette,
    eventTpl
) {

    var eventView = Marionette.ItemView.extend({

        template: _.template(eventTpl),

        className: "event",

        animatedIn: false,

        events: {
            'click #delete' : 'deleteEvent',
            'click #view' : 'viewEvent'
        },

        animateIn: function(i) {
            if(!this.animatedIn) {
                this.$el.delay(i * 100).velocity({
                    opacity: 1
                }, 500, 'easeIn')
                this.animatedIn = true;
            }
        },

        deleteEvent: function() {
            this.model.destroy({
                success: function() {

                },
                error: function() {

                }
            })
        },

        viewEvent: function(e) {
            Backbone.history.navigate('/events/' + $(e.currentTarget).data("id"), {trigger: true})
        }

    })

    return eventView;

})