<div class="event-info">
    <div class="left-info">
        <div class="title"> <p><%= title %></p></div>
        <div class="event-buttons">
            <div class="button" id="view" data-id="<%=id%>"><p>view event</p></div>
            <% if (window.localStorage.getItem("userId") == user_id) { %>
                <div class="button" id="delete" data-id="<%=id%>"><p>delete</p></div>
            <% } %>
        </div>
    </div>
    <div class="right-info">
        <div class="info description"> <p><%= description %></p> </div>

        
        <div class="info location"> <p>at <span><%= location %></span></p> </div>
        <div class="info time"> <p>on <span><%= time.split('T')[0] %></span> at <span> <%= time.split('T')[1] %>.</span></p></div>
        <div class="info capacity"> <p><span><%=cur_tickets%>/<%= max_tickets %></span> tickets left</p> </div>
    </div>
</div>