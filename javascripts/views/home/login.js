define(['jquery',
    'underscore',
    'marionette',
    'velocity',
    'ui/notification',
    'ui/form',
    'text!static/home/login.html'
], function($,
    _,
    Marionette,
    Velocity,
    Notif,
    UIForm,
    loginTpl
) {

    var loginView = Marionette.ItemView.extend({

        template: _.template(loginTpl),

        className: 'login',

        events: {
            'submit #login-form':'submitLogin',
            'click #login-button':'submitLogin',
            'click #sign-up-button':'signup'
        },

        onRender: function() {
            var view = this;
            this.$el.css('opacity', 0);
            window.setTimeout(function() {
                view.$el.css('margin-top', (($(window).height() - $(".form-wrapper").height()) / 2) - $('#title').height() / 3);    
                view.$el.velocity({
                    opacity: 1
                }, 400)
            }, 1)
        },

        submitLogin: function(e) {
            e.preventDefault()
            if(!UIForm.validateFields(["email","password"])) 
                return;
            this.postLogin();
        },

        postLogin: function() {
            $.ajax({
                url: "/api/login",
                type: "POST",
                dataType: "json",
                data: {
                    email: $("#email").val(),
                    password: $("#password").val()
                },
                success: function(data) {
                    Notif.botSuccess("Welcome back", 3000);
                    console.log(data);
                    localStorage.setItem("userId", data["id"]);
                    Backbone.history.navigate("dashboard", {trigger: true});
                },
                error: function(header, status, error) {
                    var code = header.status
                    if(code == 404) {
                        Notif.botError("Invalid Login", 1000)
                        UIForm.fieldAnimate(["email","password"])
                    } else if(code == 400) {
                        Notif.botError(header["responseText"], 2000)
                        UIForm.fieldAnimate(["email"])
                    } else if(code == 401) {
                        Notif.botError(header["responseText"], 2000)
                        UIForm.fieldAnimate(["password"])
                    }
                }
            })
        },

        signup: function() {
            Backbone.history.navigate("signup", {trigger: true})
        }

    })

    return loginView;

})