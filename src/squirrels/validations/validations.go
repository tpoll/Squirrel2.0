// Validates if email is valid, user exists, and tickets exists
package validations

import (
	"database/sql"
	"regexp"
	"squirrels/db"
	"squirrels/record"
)

const _EXP_EMAIL = `^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`

func EmailValid(e string) bool {
	exp, _ := regexp.Compile(_EXP_EMAIL)
	return exp.MatchString(e)
}

func UserExistsWithEmail(u *record.User, email string, dbLink *sql.DB) bool {
	db.FindUserByStringField(u, dbLink, "Email", email)
	if u.Email != "" {
		return true
	} else {
		return false
	}
}

func TicketExists(u_id int, e_id int, dbLink *sql.DB) bool {
	var t record.Ticket
	if db.TicketByEventAndUser(&t, u_id, e_id, dbLink) {
		return true
	}
	return false
}
