define(['jquery',
    'underscore',
    'marionette',
    'text!templates/events/attendee.tpl'
], function($,
    _,
    Marionette,
    attendeeTpl
) {

    var attendeeView = Marionette.ItemView.extend({

        className: "attendee",

        template: _.template(attendeeTpl),

        onRender: function() {
            this.checkIfCheckedIn()
        },

        modelEvents: {
            "change": "render"
        },

        checkIfCheckedIn: function() {
            if(this.model.get("ticket")["check_in"]) {
                this.$el.addClass('checked-in')
            }
        },

        events: {
            "click .check-in" : "checkIn"
        },

        checkIn: function() {
            var view = this;
            this.model.get("ticket")["check_in"] = true;
            this.$el.addClass("checked-in")
            $.ajax({
                url: "/api/events/" + view.model.get("ticket")["event_id"] + "/checkin",
                type: "post",
                dataType: "json",
                data: {
                    "user_id":view.model.get("ticket")["user_id"]
                }

            })
        }

    })

    return attendeeView

})