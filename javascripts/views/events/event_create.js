define(['jquery',
    'underscore',
    'marionette',
    'velocity',
    'models/event',
    'ui/notification',
    'ui/form',
    'vent',
    'text!static/events/event_create.html'
], function($,
    _,
    Marionette,
    Velocity,
    Event,
    UINotification,
    UIForm,
    Vent,
    eventCreateTpl
) {

    var eventCreate = Marionette.ItemView.extend({

        template: _.template(eventCreateTpl),

        ui: {
            button : '#create-button',
            form : '#event-create-form',
            cancel : '#cancel-button',
        },

        events: {
            'click @ui.button' : 'showForm',
            'click @ui.cancel' : 'hideForm',
            'submit #event-create-form' : 'submitCreateEvent',
            'click #submit-button' : 'submitCreateEvent'

        },

        onRender: function() {
            this.ui.form.hide();
        },

        showForm: function() {
            var view = this;
            $(this.ui.button).velocity({
                opacity: 0,
            }, 200, function() {
                view.ui.button.hide();   
                view.ui.form.show();
                $('.input-cell').each(function(i, d) {
                    $(d).velocity({
                        translateY: 10
                    }, 0).delay(i * 100).velocity({
                        translateY: 0,
                        opacity: 1
                    }, 400)
                })
                $('.buttons').velocity({
                    translateX: -20
                }, 0).delay(800).velocity({
                    translateX: 0,
                    opacity: 1
                }, 400, 'ease') 
            })
            
        },

        hideForm: function() {
            var view = this;
            $('.input-cell').each(function(i, d) {
                $(d).delay(i * 100).velocity({
                    translateY: -10,
                    opacity: 0
                }, 250, function() {
                    if(i == $('.input-cell').length - 1) {
                        view.ui.form.hide();      
                        view.ui.button.show();
                        view.clearForm();
                        $(view.ui.button).velocity({
                            opacity: 1
                        }, 200)
                        
                    }
                })
            })
            $('.buttons').velocity({
                translateX: -20,
                opacity: 0
            }, 200)
            
            
        },

        clearForm: function() {
            $('input, textarea').val('')
        },

        submitCreateEvent: function(e) {
            e.preventDefault();
            if(!UIForm.validateFields(["title","time","description","max-tickets","location"])) return;
            this.postCreateEvent()
        },

        postCreateEvent: function() {
            var view = this;
            UINotification.topSuccess("Event Created", 3000)
            view.hideForm();
            
            var newEvent = new Event({
                "time": $('#time').val(),
                "max_tickets": parseInt($('#max-tickets').val(), 10),
                "location": $("#location").val(),
                "description": $("#description").val(),
                "user_id": parseInt(window.localStorage.getItem("userId"), 10),
                "title": $('#event-title').val()
            });

            newEvent.save({}, {
                success: function(model, response) {
                    newEvent.set("cur_tickets", model.get("max_tickets"))
                    Vent.trigger("event-created", model)
                }, 
                error: function(model, response) {
                    UINotification.topError("Could not create event", 3000)
                }
            })
        }

    })

    return eventCreate;

})