define(['jquery',
    'underscore',
    'marionette',
    'vent',
    'models/user',
    'collections/events',
    'views/events/events',
    'views/events/event_create',
    'text!static/dashboard/dashboard_layout.html'
], function($,
    _,
    Marionette,
    Vent,
    User,
    EventsCollection,
    eventsView,
    eventCreate,
    dashLayoutTpl
) {

    var dashboard = Marionette.LayoutView.extend({

        id: 'dashboard',

        template: _.template(dashLayoutTpl),

        regions: {
            eventCreate: '#event-create',
            events: '#user-events',
            attending: '#user-attending'
        },

        initialize: function() {
            var view = this;
            this.listenTo(Vent, "event-created", function(model) {
                view.events.currentView.collection.add(model)
                view.events.currentView.animateNew()
            })

        },

        onRender: function() {
            
            this.renderUserEvents();
            this.eventCreate.show(new eventCreate());
            

        },

        renderUserEvents: function() {
            var view = this;
            var events = new EventsCollection();
            events.fetch({
                url: '/api/events',
                data: {
                    "user_id" : window.localStorage.getItem("userId")
                },
                success: function(events) {
                    console.log(events)
                    view.events.show(new eventsView({collection: events, title: "My Events"}))
                }
            })
        }

    })

    return dashboard

})