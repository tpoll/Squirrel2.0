/* 
 * file       : main.js
 *
 * type       : Javascript Module
 * 
 * represents : Initializer script
 *
 * notes      : sets requirejs configuration
 *              inits backbone router
 *              kicks off history
 *
 */

"use strict";

require.config({
    paths: {
        marionette   : 'libs/marionette',
        backbone     : 'libs/backbone',
        jquery       : 'libs/jquery',
        underscore   : 'libs/underscore',
        text         : 'libs/text',
        vent         : 'libs/vent',
        velocity     : 'libs/velocity',
        cookie       : 'libs/jquery.cookie'
    },
})

require(['jquery',
    'routers/home',
    'views/general/navigation',
    'views/general/site_layout'
], function($,
    homeRouter,
    navigationView,
    siteLayout
){
    var home = new homeRouter();
    window.Layout = new siteLayout();
    $('body').append(window.Layout.render().el);
    window.Layout.navigation.show(new navigationView());
    Backbone.history.start({pushState: true, root: '/'})
    
})