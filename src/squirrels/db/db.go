// Package the implements CRUD methods for each of our databases
// and some other databse query fucntions.
package db

import (
	"bytes"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"reflect"
	"squirrels/errors"
	"squirrels/record"
)

func GetStructFields(v interface{}) map[string]string {
	s := reflect.ValueOf(v)
	typeOfT := s.Type()
	Fields := make(map[string]string)

	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		Fields[typeOfT.Field(i).Name] = f.Type().Name()
	}
	return Fields

}

func AddTable(v interface{}, TableName string) {
	var buffer bytes.Buffer
	count := 0
	Fields := GetStructFields(v)
	s := fmt.Sprintf("create table if not exists %s(", TableName)
	buffer.WriteString(s)
	size := len(Fields)
	db, _ := sql.Open("postgres", "postgres://xuihgygrphqoas:iSfrBpnf3ybhnmT3aZl-EVDzem@ec2-107-20-159-155.compute-1.amazonaws.com:5432/d9vbco1jv14e38")

	for k, v := range Fields {
		count++
		if v == "string" {
			v = "text"
		}
		if k == "Id" {
			v = "serial primary key"
		}

		if count == size {
			buffer.WriteString(fmt.Sprintf("%s %s)", k, v))
		} else {
			buffer.WriteString(fmt.Sprintf("%s %s, ", k, v))
		}
	}

	db.Exec(buffer.String())
}

// ********************** User CRUD  ****************************************
func CreateUser(u *record.User, db *sql.DB) bool {
	var userid int
	println(u.IdCard)
	err := db.QueryRow("INSERT INTO users(Email, First, Last, PassHash, IdCard)"+
		"VALUES($1, $2, $3, $4, $5) RETURNING Id", u.Email, u.First,
		u.Last, u.PassHash, u.IdCard).Scan(&userid)
	return !errors.IsErr(err)
}

func ReadUser(u *record.User, db *sql.DB, id int) bool {
	err := db.QueryRow("SELECT Id, Email, First, Last, PassHash, IdCard FROM users"+
		" WHERE Id=$1", id).Scan(&u.Id, &u.Email, &u.First, &u.Last,
		&u.PassHash, &u.IdCard)
	return !errors.IsErr(err)
}

func UpdateUser(u *record.User, db *sql.DB) bool {
	_, err := db.Exec("UPDATE users SET Email=$1, First=$2, Last=$3"+
		"  WHERE Id=$4", u.Email, u.First, u.Last, u.Id)
	return !errors.IsErr(err)
}

func DestroyUser(id int, db *sql.DB) bool {
	_, err := db.Exec("DELETE FROM users WHERE Id=$1", id)
	return !errors.IsErr(err)
}

func FindUserByCardId(u *record.User, db *sql.DB, id_card string) bool {
	err := db.QueryRow("SELECT Id, Email, First, Last, PassHash, IdCard FROM users"+
		" WHERE IdCard=$1", id_card).Scan(&u.Id, &u.Email, &u.First,
		&u.Last, &u.PassHash, &u.IdCard)
	return !errors.IsErr(err)
}

func FindUserByStringField(u *record.User, db *sql.DB, field string, value string) bool {
	err := db.QueryRow("SELECT Id, Email, First, Last, PassHash, IdCard FROM users"+
		" WHERE "+field+"=$1", value).Scan(&u.Id, &u.Email, &u.First,
		&u.Last, &u.PassHash, &u.IdCard)
	return !errors.IsErr(err)
}

// ******************** End User CRUD ***************************************

// ******************** Event Crud ******************************************

func CreateEvent(e *record.Event, db *sql.DB) bool {
	AddTable(record.Event{}, "events")
	var eventid int
	// println(e.Time)
	err := db.QueryRow("INSERT INTO events(Time, Location, Description,"+
		"MaxTickets, UserId, Title, CurTickets) VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING Id",
		e.Time, e.Location, e.Description, e.MaxTickets, e.UserId,
		e.Title, e.MaxTickets).Scan(&eventid)
	e.Id = eventid
	return !errors.IsErr(err)
}

func ReadEvent(e *record.Event, db *sql.DB, id int) bool {
	err := db.QueryRow("SELECT Id, Time, Location, Description, MaxTickets, "+
		"UserId, Title, CurTickets FROM events WHERE Id=$1", id).Scan(&e.Id, &e.Time,
		&e.Location, &e.Description, &e.MaxTickets, &e.UserId,
		&e.Title, &e.CurTickets)
	return !errors.IsErr(err)
}

func UpdateEvent(e *record.Event, db *sql.DB) bool {
	_, err := db.Exec("UPDATE events SET Time=$1, Location=$2, Description=$3,"+
		" MaxTickets=$4, UserId=$5, Title=$6, CurTickets=$7 WHERE Id=$8", e.Time, e.Location,
		e.Description, e.MaxTickets, e.UserId, e.Title, e.CurTickets, e.Id)
	return !errors.IsErr(err)
}

func DestroyEvent(db *sql.DB, id int) bool {
	_, err := db.Exec("DELETE FROM events WHERE Id=$1", id)
	return !errors.IsErr(err)
}

func ReadEventsCreated(db *sql.DB, uid int) []record.Event {
	rows, _ := db.Query("SELECT Id, Time, Location, Description, MaxTickets, UserId, "+
		"Title, CurTickets FROM events WHERE UserId=$1", uid)
	events := []record.Event{}
	for rows.Next() {
		e := record.Event{}
		rows.Scan(&e.Id, &e.Time, &e.Location, &e.Description,
			&e.MaxTickets, &e.UserId, &e.Title, &e.CurTickets)
		events = append(events, e)
	}
	println(len(events))
	rows.Close()
	return events
}

func ReadEventsAll(db *sql.DB) []record.Event {
	rows, _ := db.Query("SELECT Id, Time, Location, Description, MaxTickets, UserId, Title, CurTickets FROM events")
	events := []record.Event{}
	for rows.Next() {
		e := record.Event{}
		rows.Scan(&e.Id, &e.Time, &e.Location, &e.Description,
			&e.MaxTickets, &e.UserId, &e.Title, &e.CurTickets)
		events = append(events, e)
	}
	rows.Close()
	return events
}

func ReadEventAttendees(db *sql.DB, id int) []record.Attendee {
	ticket_rows, _ := db.Query("Select Id, UserId, EventId, CheckedIn FROM tickets WHERE EventId=$1", id)
	attendees := []record.Attendee{}
	for ticket_rows.Next() {
		t := record.Ticket{}
		u := record.User{}
		a := record.Attendee{}
		ticket_rows.Scan(&t.Id, &t.UserId, &t.EventId, &t.CheckedIn)
		ReadUser(&u, db, t.UserId)
		u.PassHash = "private"
		a.Person = u
		a.Info = t
		a.UserId = u.Id
		attendees = append(attendees, a)
	}
	ticket_rows.Close()
	return attendees
}

// ********************** End Event CRUD*************************************

// ********************** Ticket CRUD ***************************************
func CreateTicket(t *record.Ticket, db *sql.DB) bool {
	_, err := db.Exec("INSERT INTO tickets(EventId, UserId, CheckedIn)"+
		"VALUES($1, $2, $3)", t.EventId, t.UserId, t.CheckedIn)
	return !errors.IsErr(err)
}

func ReadTicket(t *record.Ticket, db *sql.DB, id int) bool {
	err := db.QueryRow("SELECT Id, EventId, UserId, CheckedIn FROM tickets"+
		" WHERE Id=$1", id).Scan(&t.Id, &t.EventId, &t.UserId, &t.CheckedIn)
	return !errors.IsErr(err)
}
func UpdateTicket(t *record.Ticket, db *sql.DB) bool {
	_, err := db.Exec("UPDATE tickets SET EventId=$1, UserId=$2, CheckedIn=$3 "+
		" WHERE Id=$4", t.EventId, t.UserId, t.CheckedIn, t.Id)
	return !errors.IsErr(err)
}

func DestroyTicket(id int, db *sql.DB) bool {
	_, err := db.Exec("DELETE FROM tickets WHERE Id=$1", id)
	return !errors.IsErr(err)
}

func TicketByEventAndUser(t *record.Ticket, u int, e int, db *sql.DB) bool {
	err := db.QueryRow("SELECT Id, EventId, UserId, CheckedIn FROM tickets"+
		" WHERE UserId=$1 and EventId=$2", u, e).Scan(&t.Id, &t.EventId,
		&t.UserId, &t.CheckedIn)
	return !errors.IsErr(err)
}

//********************* End Ticket CRUD ************************************
