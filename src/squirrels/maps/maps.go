// Concurrenct ticket map that impelemts the deletion of an event from
// the map and getting a ticket
package maps

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"log"
	"sync"
)

type Semaphore struct {
	Avail int
	Ch    chan int
	Mutex *sync.Mutex
}

type ThMap struct {
	Tickets map[int]Semaphore
	RwMut   *sync.RWMutex
}

func MakeEvent(m *ThMap, id int, numTickets int) {
	m.RwMut.Lock()
	defer m.RwMut.Unlock()
	m.Tickets[id] = Semaphore{numTickets, make(chan int, numTickets), &sync.Mutex{}}
}

func DeleteEvent(m *ThMap, id int) {
	m.RwMut.Lock()
	defer m.RwMut.Unlock()
	delete(m.Tickets, id)
}

func GetTicket(m *ThMap, id int, dbLink *sql.DB) error {
	m.RwMut.Lock()
	defer m.RwMut.Unlock()
	sem := m.Tickets[id]

	sem.Mutex.Lock()
	if sem.Avail <= 0 {
		sem.Mutex.Unlock()
		return errors.New("No Tickets")
	} else {
		sem.Avail--
		sem.Ch <- 1
		sem.Mutex.Unlock()

	}
	m.Tickets[id] = sem
	go UpdateCurTicket(dbLink, id, sem.Avail)
	log.Print("done")
	return nil
}

func InitMaps(m *ThMap, dbLink *sql.DB) {
	var id int
	var ticks int

	rows, err := dbLink.Query("SELECT Id, CurTickets FROM events")
	defer rows.Close()

	if err != nil {
		log.Fatal("database could not load")
	}

	for rows.Next() {
		rows.Scan(&id, &ticks)
		m.Tickets[id] = Semaphore{ticks, make(chan int, ticks), &sync.Mutex{}}
	}

}

func UpdateCurTicket(dbLink *sql.DB, id int, cur int) {
	_, err := dbLink.Exec("UPDATE events SET CurTickets=$1  WHERE Id=$2", cur, id)
	if err != nil {
		log.Print("failed to update")
	}
}
