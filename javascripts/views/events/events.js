define(['jquery',
    'underscore',
    'marionette',
    'velocity',
    'views/events/event',
    'text!static/events/events.html'
], function($,
    _, 
    Marionette,
    Velocity,
    eventView,
    eventsTpl
) {

    var eventsView = Marionette.CompositeView.extend({

        template: _.template(eventsTpl),

        childView: eventView,

        className: "events-view",

        initialRender: false,

        childViewContainer: ".events-container",

        initialize: function(options) {
            this.options = options;
        },

        onRender: function() {
            var view = this;
            if(!this.initialRender) {
                window.setTimeout(function() {
                    if(view.options["title"]) {
                        $('.event-list-title').html(view.options["title"])
                    } else {
                        $('.event-list-title, .separator').remove();
                    }
                    $('.event-list-title').velocity({
                        opacity: 1
                    }, 400)
                    $('.separator').delay(50).velocity({
                        opacity: 1,
                        width: "100%"
                    }, 300, 'easeOutCubic')
                }, 10);
                this.children.each(function(d, i) {
                    d.animateIn(i);
                })
                this.initialRender = true;
            }
        },

        animateNew: function() {
            this.children.each(function(d) {
                d.animateIn(0)
            })
        }

    })

    return eventsView;

})