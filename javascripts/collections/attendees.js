define(['jquery',
    'backbone'
], function($,
    backbone
) {

    var attendee = Backbone.Collection.extend({

        comparator: function(model) {
            return -model.get("ticket")["check_in"]
        }

    })

    return attendee;

})