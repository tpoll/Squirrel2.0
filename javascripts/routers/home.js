define(['jquery',
    'marionette',
    'controllers/home'
], function($,
    Marionette,
    homeController
) {

    var router = Marionette.AppRouter.extend({

        controller: new homeController(),

        appRoutes: {
            ''      : 'login',
            'login' : 'login',
            'signup': 'signup',
            'logout': 'logout',
            'dashboard' : 'dash',
            'events' : 'events',
            'events/:id' : 'event'
        }

    })

    return router

})