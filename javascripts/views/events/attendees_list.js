define(["jquery",
    "underscore",
    "marionette",
    "views/events/attendee",
    "text!static/events/attendees.html"
], function($,
    _,
    Marionette,
    attendeeView,
    attendeesTpl
) {

    var attendeesList = Marionette.CompositeView.extend({

        template: _.template(attendeesTpl),

        childView: attendeeView,

        childViewContainer: "#attendees-container",

        className: "attendee-list",

        initialize: function(options) {
            this.id = options["id"]
        },

    })

    return attendeesList;

})