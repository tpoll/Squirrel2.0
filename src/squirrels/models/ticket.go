// Part of the models package converting the ticket struct to and from json
package models

import (
	"encoding/json"
	"log"
	"squirrels/errors"
	"squirrels/record"
)

func TicketFromJSON(jsonString string, t *record.Ticket) {
	err := json.Unmarshal([]byte(jsonString), t)
	if errors.IsErr(err) {
		log.Fatalln("Could not Unmarshal Ticket JSON")
	}
}

func TicketToJSON(t record.Ticket) []byte {
	json, err := json.Marshal(t)
	if errors.IsErr(err) {
		log.Fatalln("Could not Marshal Ticket Object")
	}
	return json
}

/* create, read, update, destroy */
