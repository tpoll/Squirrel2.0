define(['underscore',
    'marionette',
    'text!static/general/site_layout.html'
], function(_,
    Marionette,
    siteLayoutTpl
) {

    var siteLayout = Marionette.LayoutView.extend({

        id: 'layout',

        template: _.template(siteLayoutTpl),

        regions: {
            navigation: '#navigation',
            content   : '#content'
        }

    })

    return siteLayout;

})  