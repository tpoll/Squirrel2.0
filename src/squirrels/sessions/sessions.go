// Package that handles session persistance after a user has been authenticated
package sessions

import (
	"database/sql"
	"github.com/gorilla/securecookie"
	"net/http"
	"squirrels/models"
	"squirrels/record"
	"squirrels/validations"
	"strconv"
)

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

// Extract email data from cookie
func GetUserId(r *http.Request) (id int) {
	if cookie, err := r.Cookie("session"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
			id, _ = strconv.Atoi(cookieValue["id"])
		}
	}
	return id

}

// Set session information in cookie and bind it to response
func setSession(id int, r http.ResponseWriter) {
	// create information to be stored
	value := map[string]string{
		"id": strconv.Itoa(id),
	}
	// encode information and bind cookie to response
	if encoded, err := cookieHandler.Encode("session", value); err == nil {
		cookie := &http.Cookie{
			Name:  "session",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(r, cookie)
	}
}

// clear session by sending blank cookie
func clearSession(r http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:  "session",
		Value: "",
		Path:  "/",
	}
	http.SetCookie(r, cookie)
}

// get credentials and authenticate user,
// setting session if credentials are valid
func Login(w http.ResponseWriter, r *http.Request, dbLink *sql.DB) {

	email := r.FormValue("email")
	pass := r.FormValue("password")

	if email != "" && pass != "" {
		if validations.EmailValid(email) {
			u := new(record.User)
			if validations.UserExistsWithEmail(u, email, dbLink) {
				if models.UserComparePassHash(pass, u.PassHash) {
					setSession(u.Id, w)
					u.PassHash = "private"
					w.Write(models.UserToJSON(*u))
				} else {
					http.Error(w, "Invalid Password", 401)
				}
			} else {
				http.Error(w, "No user with email "+email, 400)
			}
		} else {
			http.Error(w, "Invalid Email Format", 400)
		}
	} else {
		http.Error(w, "Bad Login", 404)
	}
}

func AuthenticateBasic(r *http.Request, db *sql.DB) bool {
	if GetUserId(r) == 0 {
		return false
	}
	return true
}

func AuthenticateStrong(r *http.Request, id int, db *sql.DB) bool {
	if (GetUserId(r)) != id {
		return false
	}
	return true
}

// logout handler
func Logout(w http.ResponseWriter) {
	clearSession(w)
	w.Write([]byte(`{"status":200,"message":"logged out"}`))
}
