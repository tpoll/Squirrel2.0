define(['jquery',
    'backbone'
], function($,
    backbone
) {

    var eventModel = Backbone.Model.extend({

        urlRoot: '/api/events'

    })

    return eventModel;

})