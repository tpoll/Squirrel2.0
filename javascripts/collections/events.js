define(['jquery',
    'backbone'
], function($,
    backbone
) {

    var events = Backbone.Collection.extend({

        url: '/api/events',

        comparator: function(model) {
            return -model.get("id")
        }

    })

    return events;

})