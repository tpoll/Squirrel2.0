// Collection of structs that map to the database objects
package record

type User struct {
	Id       int    `json:"id, omitempty"`
	Email    string `json:"email"`
	First    string `json:"first"`
	Last     string `json:"last"`
	IdCard   string `json:"-"`
	PassHash string `json:"-"`
}

type Event struct {
	Id          int    `json:"id"`
	Time        string `json:"time"`
	Location    string `json:"location"`
	Description string `json:"description"`
	MaxTickets  int    `json:"max_tickets"`
	UserId      int    `json:"user_id"`
	Title       string `json:"title"`
	CurTickets  int    `json:"cur_tickets"`
}

type Ticket struct {
	Id        int  `json:"id"`
	EventId   int  `json:"event_id"`
	UserId    int  `json:"user_id"`
	CheckedIn bool `json:"check_in"`
}

type Attendee struct {
	UserId int    `json:"id"`
	Person User   `json:"person"`
	Info   Ticket `json:"ticket"`
}
