define(['jquery',
    'underscore',
    'marionette',
    'models/user',
    'velocity',
    'text!static/general/navigation.html'
], function($,
    _,
    Marionette,
    User,
    Velocity,
    navTpl
) {

    var navView = Marionette.ItemView.extend({

        id: "navigation-view",

        className: "disappeared",

        template: _.template(navTpl),

        events: {
            'click .nav-item' : 'navigate'
        },

        select: function(which) {
            $('.nav-item[data-action='+which+']').addClass('selected')
        },

        navigate: function(e) {
            if($(e.currentTarget).hasClass('selected')) {
                return;
            } else {
                $('.selected').removeClass('selected')
                $(e.currentTarget).addClass('selected');
            }
            Backbone.history.navigate($(e.currentTarget).data('action'), {trigger: true});
        },

        disappear: function() {
            var view = this;
            this.$el.find(".nav-item").each(function(i, d) {
                $(d).delay(50 * i).velocity({
                    opacity: 0
                }, 100, function() {
                    view.$el.css('display', 'none')
                });
            })
            $('.current-user').velocity({opacity: 0}, 100)
        },

        appear: function(selected) {
            $('.selected').removeClass('selected');
            this.$el.find('.nav-item[data-action='+selected+']').addClass('selected');
            this.$el.css('display', 'block');
            this.$el.find(".nav-item").each(function(i, d) {
                $(d).delay(100 * i).velocity({
                    opacity: 1
                }, 300, function() {
                    
                });
            })
            var uid = window.localStorage.getItem("userId");
            if(uid) {
                var user = new User({id: uid})
                user.fetch({
                    data: {
                        "user_id": uid
                    },
                    success: function() {
                        $(".current-user").html(user.get("first") + " " + user.get("last")).velocity({opacity: 1}, 1000)
                    }
                })
            }
        }

    })

    return navView;

})