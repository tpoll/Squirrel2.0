define(['jquery',
    'underscore',
    'marionette',
    'models/user',
    'models/attendee',
    'ui/notification',
    'vent',
    'text!templates/events/event_expanded.tpl'
], function($,
    _,
    Marionette,
    User,
    Attendee,
    UINotifs,
    Vent,
    eventTpl
) {

    var eventExpandedView = Marionette.ItemView.extend({

        template: _.template(eventTpl),

        className: "event-expanded",

        events: {
            "click #join": "joinEvent"
        },

        modelEvents: {
            "change": "render"
        },

        host: null,

        onBeforeDestroy: function() {
            this.conn.onmessage = function(evt) {
                return
            }
        },

        onRender: function() {
            var view = this;
            
            if(this.host == null) {
                this.host = new User({id: this.model.get("user_id")})
                this.host.fetch({
                    success: function(model) {
                        $(".host").html(model.get("first") + " " + model.get("last"))
                    }
                })

            } else {
                $(".host").html(this.host.get("first") + " " + this.host.get("last"))
            }

            if (window["WebSocket"]) {
                this.conn = new WebSocket("ws://104.131.126.99/ws");
                // view.conn = new WebSocket("ws://localhost:3000/ws");
                view.conn.onclose = function(evt) {
                    console.log("connection closed")
                }
                view.conn.onmessage = function(evt) {
                    data = JSON.parse(evt["data"])

                    if(data["ticket"] && data["ticket"]["check_in"] && data["ticket"]["event_id"] == view.model.get("id")) {
                        var attendee = new Attendee(data)
                        UINotifs.topSuccess(attendee.get("person")["first"] + " " + attendee.get("person")["last"] + " checked in", 3000)
                        Vent.trigger("checked-in", attendee)
                    } else if(data["ticket"] && data["ticket"]["event_id"] == view.model.get("id")) {
                        var attendee = new Attendee(data)
                        UINotifs.topSuccess(attendee.get("person")["first"] + " " + attendee.get("person")["last"] + " took ticket", 3000)
                        Vent.trigger("ticket-obtained", attendee)
                    }
                }

            } else {
                console.log($("<div><b>Your browser does not support WebSockets.</b></div>"))
            }
        },

        joinEvent: function() {
            var view = this;
            $.ajax({
                url: "/api/events/" + view.model.get('id') + "/ticket",
                type: "POST",
                dataType: "json",
                data: {
                    user_id: window.localStorage.getItem("userId")
                },
                success: function(data) {
                    UINotifs.topSuccess("Ticket Reserved", 3000)
                }, 
                error: function(header, status, error) {
                    UINotifs.topError(header["responseText"], 3000)
                }
            })
        },

    })

    return eventExpandedView;

})