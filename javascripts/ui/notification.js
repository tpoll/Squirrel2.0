define(['jquery',
    'velocity'
], function($,
    Velocity
) {

    var createError = function(message) {
        return $("<div></div>").addClass('message error-message').html("<p>" + message + "</p>");
    }

    var createSuccess = function(message) {
        return $("<div></div>").addClass('message success-message').html("<p>" + message + "</p>");   
    }

    var notify = function(message, duration, type, position) {
        var $message, $element;
        switch(position) {
            case "top":
                $element = $("<div class='message-top'></div>");
                break;
            case "bottom":
                $element = $("<div class='message-bot'></div>")
                break;
        }
        switch(type) {
            case "error":
                $message = $element.html(createError(message));
                break;
            case "success":
                $message = $element.html(createSuccess(message));
                break;
        }
        $('body').prepend($message);
        $message.velocity({
            opacity: 0
        }, 0).velocity({
            opacity: 1
        }, 400).delay(duration).velocity({
            opacity: 0
        }, 400, function() {
            $message.remove();
        })
    }

    var notification = {

        topError: function(message, duration) {
            notify(message, duration, "error", "top")
        },

        topSuccess: function(message, duration) {
            notify(message, duration, "success", "top")
        },

        botError: function(message, duration) {

            notify(message, duration, "error", "bottom");
        },

        botSuccess: function(message, duration) {
            notify(message, duration, "success", "bottom");
        }

    }

    return notification

})