define(['jquery',
    'underscore',
    'marionette',
    'vent',
    'collections/attendees',
    'views/events/event_expanded',
    'views/events/attendees_list',
    'text!static/events/event_page_layout.html'
], function($,
    _,
    Marionette,
    Vent,
    Attendees,
    eventView,
    attendeesView,
    eventPageTpl
) {

    var eventPageLayout = Marionette.LayoutView.extend({

        template: _.template(eventPageTpl),

        regions: {
            details: "#event-details",
            attendees: '#attendees'
        },

        onRender: function() {
            var view = this;
            this.attendeesCollection = new Attendees();

            this.listenTo(Vent, "ticket-obtained", function(attendee) {
                view.attendeesCollection.add(attendee)
                var cur_tickets = view.model.get("cur_tickets") - 1;
                view.model.set("cur_tickets", cur_tickets)
            })

            this.listenTo(Vent, "checked-in", function(attendee) {
                var model = view.attendeesCollection.get(attendee.get("id"))
                model.set("ticket", attendee.get("ticket"))
            })

            this.model.fetch({
                success: function(model) {
                    view.details.show(new eventView({model: model}))
                }
            })

            this.attendeesCollection.fetch({
                url: "/api/events/" + view.model.id + "/attendees",
                success: function(collection) {
                    console.log(collection)
                    view.attendees.show(new attendeesView({collection: collection, id: view.model.get("id")}))
                }
            })
        },


    })

    return eventPageLayout;

})