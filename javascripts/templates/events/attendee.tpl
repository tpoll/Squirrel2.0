<div class="attendee-info">
    <p><%= person["first"] + " " + person["last"] %></p>
</div>
<div class="checked-in">
    <p><% if(ticket["check_in"]) {  %>
        Checked In
    <% } else { %>
        Registered
    <% } %></p>
</div>